<html>
    <head>
        <title>Yan Ying</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="../CSS/TracyCSS.css"<?php echo time(); ?>/>
        <script src="../JS/TracyJS.js"></script>
    </head>
    <body>

        <div id="pageContent">
            <!-- Navigation -->
            <div id="navBar">
                <a href="#about_me" class="navLinks">About Me</a>
                <a href="#technical_skills" class="navLinks">Skills</a>
                <a href="#work_experiences" class="navLinks">Work Experiences</a>
                <a href="#my_projects" class="navLinks">My Projects</a>
            </div>



            <div id="tracyContents">            
                <!-- Greetings -->
                <section id="greetings">
                    <p><span id="name">Yan Ying Ling</span><span>&lt;Welcome to my world!&sol;&gt</span></p>
                </section>
                <!-- About Me -->
                <section>
                    <a class="anchor"  id="about_me"></a>
                    <h2>About Me</h2>
                    <p>
                        Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.
                        Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.
                        Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.
                        Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.
                        Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.
                        Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.
                        Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.Hi, it's me.
                    </p>
                </section>
                <!-- My Skills -->
                <section>
                    <a class="anchor"  id="technical_skills"></a>
                    <h2>Technical Skills</h2>
                    <p><div class="prog_langs">Java</div></p>
                    <p>C++</p>
                    <p>PHP</p>
                    <p>Java</p>
                    <p>Java</p>
                    <p>Java</p>
                    <p>Java</p>
                    <p>Java</p>
                    <p>Java</p>
                    <p>Java</p>
                    <p>Java</p>
                    <p><div class="other_techs">Microsoft Word</div><div class="other_techs">Microsoft Power Point</div></p>
                    <p>a</p>
                    <p>Java</p>
                    <p>Java</p>
                    <p>Java</p>
                    <p>Java</p>
                    <p>Java</p>
                    <p>Java</p>
                    <p>Java</p>
                    <p>Java</p>
                </section>
                <!-- Work Experiences -->
                <section>
                    <a class="anchor" id="work_experiences"></a>
                    <h2 >My Work Experience</h2>
                    <p class="jobs">
                    <div class="job_title">Test Engineer <span>@ Pactera</span></div>
                    <div class="job_description">Testing of functions like account creation and report generation for the software in our client company (UOB).</div>
                    </p>
                    <p class="jobs">
                    <div class="job_title">Job Title 2</div>
                    <div class="job_description">Some job descriptions</div>
                    </p>
                    <p class="jobs">
                    <div class="job_title">Job Title 2</div>
                    <div class="job_description">Some job descriptions</div>
                    </p>
                    <p class="jobs">
                    <div class="job_title">Job Title 2</div>
                    <div class="job_description">Some job descriptions</div>
                    </p>
                </section>

                <!-- My Projects (Any Coding Projects) -->
                <section>
                    <a class="anchor" id="my_projects"></a>
                    <h2>My Projects</h2>
                    <div>
                        Currently None, WIP
                    </div>
                    <div>
                        Currently None, WIP
                    </div>
                    <div>
                        Currently None, WIP
                    </div>
                    <div>
                        Currently None, WIP
                    </div>
                </section>
            </div>

            <!-- Footer (Social Contact Informations) -->
            <footer  id="footer">
                <div class="sns_icons">
                    <a href="https://github.com/tracyling98" class="fa fa-github"></a>
                    <a href="https://www.linkedin.com/in/tracyling98" class="fa fa-linkedin"></a>
                    <a href="mailto:yanying25@outlook.com" class="fa fa-envelope"></a>


                </div>
            </footer>
        </div>
    </body>
</html>
